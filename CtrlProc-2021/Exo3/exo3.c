#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int p_lettre[2];
int p_chiffre[2];
int nb_lettre_lu;
int resultat;

void fils1(void);
void fils2(void);
void termine(int signum);

int main(int argc, char *argv[]) {
    pid_t pid;
    char c;
    int digit;

    if (pipe(p_lettre) != 0) {
        perror("Problème avec pipe\n");
        exit(2);
    }

    if ((pid = fork()) == 0) {
        fils1();
    } else if (pid == -1) {
        perror("Une erreur est survenu avec le fork\n");
        exit(2);
    }
    /*
    if ((pid = fork()) == 0) {
        fils2();
    }*/

    while ((c = getchar()) != '0') {
        printf("caractère reçu : %c\n", c);
        close(p_lettre[0]);

        // Tri des lettres
        if ( (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') ) {
            write(p_lettre[1], &c, 1);
        }
        close(p_lettre[1]);

        // Tri des chiffres
        if (digit >= '0' && digit <= '9') {
            //write(p_chiffre[1], &digit, sizeof(int));
        }
    }

    sleep(1);

    // Le père à fini et le fait savoir à son fils
    kill(SIGUSR1, pid);

    sleep(1);

    printf("Le nombre de lettre lu est : %d\n", nb_lettre_lu);

    wait(NULL);
    //wait(NULL);


    return EXIT_SUCCESS;
}


void fils1(void) {
    sig_t s1;
    char lettre;
    int i = 0;

    s1 = signal(SIGUSR1, &termine);

    close(p_lettre[1]);
    while(read(p_lettre[0], &lettre, 1) != 0) {
        printf("Je fais i++");
        i++;
    }
    close(p_lettre[0]);

    pause();

    nb_lettre_lu = i;
    exit(EXIT_SUCCESS);
}

void fils2(void) {
    
}

void termine(int signum) {
    printf("Envoi du nombre d'occurrences...\n");
}