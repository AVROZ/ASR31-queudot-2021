#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    pid_t pid, pid2;
    pid_t numproc;

    pid = fork();
    if (pid2 == -1) {
        perror("Un problème est survenu lors du fork\n");
        exit(2);
    }

    if (pid == 0) {
        // Exec de ls -l
        if (execlp("ls", "ls", "-l", NULL) == -1) {
            perror("Un problème est survenu lors du exec\n");
            exit(0);
        }
    } else {
        pid2 = fork();
        if (pid2 == -1) {
            perror("Un problème est survenu lors du fork\n");
            exit(2);
        }

        if (pid2 == 0) {
            // Exec de ps -l
            if (execlp("ps", "ps", "-l", NULL) == -1) {
                perror("Un problème est survenu lors du exec\n");
                exit(0);
            }
        }

        // Enregistrement du processus qui s'est terminé en premier
        numproc = wait(NULL);
        wait(NULL);

        printf("Le processus qui s'est terminé en premier est le : %d\n", numproc);
    }

    return EXIT_SUCCESS;
}